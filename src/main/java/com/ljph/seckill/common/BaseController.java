package com.ljph.seckill.common;

import com.jfinal.core.Controller;
import com.ljph.seckill.dto.SeckillResult;

/**
 * Created by yuzhou on 16/9/1.
 */
public class BaseController extends Controller{

    protected void commonSuccessJson() {
        renderJson(new SeckillResult<Object>(true));
    }
}
